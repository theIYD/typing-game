import React, { useState } from 'react';
import styled from 'styled-components';
import { fetchRequest } from '../../utils/fetch';

import Modal from '../Modal';
import Button from '../Button';
import Loader from '../Loader';

const Flex = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  flex-direction: ${({ flexDirection }) => flexDirection || 'row'};
`;

const StyledInput = styled.input`
  border-radius: 0.3125rem;
  background-color: #f2f2f2;
  color: black;
  outline: 0;
  border: none;
  padding-left: 0.75rem;
  padding-right: 0.75rem;
  letter-spacing: -0.021rem;
  flex: 1;
  height: 2.5rem;
  margin-top: 0.5rem;
`;

function User({ isOpen, onClose }) {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [isSubmitting, setIsSubmitting] = useState(false);

  const submitHandler = async (e) => {
    e.preventDefault();
    setIsSubmitting(true);

    const data = await fetchRequest(
      `${process.env.REACT_APP_API}/user`,
      'POST',
      { name, email }
    );

    if (data.error === 0 && data.token) {
      setIsSubmitting(false);
      localStorage.setItem('token', data.token);
      onClose();
    }
  };

  return (
    <Modal shouldCloseOnOverlayClick={false} isOpen={isOpen} onClose={onClose}>
      {isSubmitting ? (
        <Loader />
      ) : (
        <form onSubmit={submitHandler}>
          <Flex>
            <StyledInput
              onChange={(e) => setName(e.target.value)}
              type="text"
              placeholder="Your name"
              required
            />
          </Flex>

          <Flex>
            <StyledInput
              onChange={(e) => setEmail(e.target.value)}
              type="email"
              placeholder="Your email"
              required
            />
          </Flex>

          <Button style={{ marginTop: '10px' }} type="submit">
            Submit
          </Button>
        </form>
      )}
    </Modal>
  );
}

export default User;
