import React from 'react';
import styled from 'styled-components';

const StyledButton = styled.button`
  border-radius: 0.3125rem;
  background-color: black;
  font-style: normal;
  line-height: 1.125rem;
  letter-spacing: -0.0208rem;
  outline: 0;
  border-width: 0;
  cursor: pointer;
  color: white;
  width: 100%;
  text-align: center;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
  padding: 0.69rem;
  min-height: 2.5rem;
  border-color: transparent;
  margin-right: 10px;
`;

function Button({ children, ...rest }) {
  return <StyledButton {...rest}>{children}</StyledButton>;
}

export default Button;
