import React from 'react';
import styled, { keyframes } from 'styled-components';

const Spin = keyframes`
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
`;

const Elem = styled.div`
  border: 2px solid #f3f3f3;
  border-top: 2px solid black;
  border-radius: 50%;
  width: 30px;
  height: 30px;
  animation: ${Spin} 500ms linear infinite;
  margin-top: 20px;
`;

function Loader() {
  return <Elem />;
}

export default Loader;
