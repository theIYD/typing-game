import React from 'react';
import styled, { css } from 'styled-components';

const Shape = styled.div`
  position: relative;
  ${({ stat }) => {
    if (stat === 'level') {
      return css`
        width: 120px;
        height: 50px;
        border-radius: 34px;
        background-color: lightblue;

        ::before {
          border-radius: 50%;
        }
      `;
    } else if (stat === 'score') {
      return css`
        border-bottom: 50px solid lightgreen;
        border-left: 25px solid transparent;
        border-right: 25px solid transparent;
        height: 0;
        width: 125px;
      `;
    } else if (stat === 'multiplier') {
      return css`
        width: 0;
        height: 0;
        border: 60px solid transparent;
        border-bottom-color: purple;
        position: relative;
        top: -35px;
        margin-left: 10px;
      `;
    }
  }};
`;

const Text = styled.p`
  color: white;
  font-weight: bold;

  ${({ additional }) =>
    additional &&
    css`
      text-transform: uppercase;
      font-size: 8px;
      letter-spacing: 2px;
    `}
`;

const Wrap = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  ${({ top }) =>
    top &&
    css`
      top: ${top};
    `};
`;

function Stat({ stat, data, css }) {
  return (
    <Shape stat={stat}>
      <Wrap top={css.top}>
        <Text>{data.stat}</Text>
        <Text additional>{data.text}</Text>
      </Wrap>
    </Shape>
  );
}

export default Stat;
