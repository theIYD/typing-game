import React from 'react';

import ReactModal from 'react-modal';

function Modal({ isOpen, onClose, customStyles, children, ...rest }) {
  const styles = {
    content: {
      top: '50%',
      left: '50%',
      right: 'auto',
      bottom: 'auto',
      marginRight: '-50%',
      transform: 'translate(-50%, -50%)',
    },
  };
  return (
    <ReactModal
      isOpen={isOpen}
      style={{ ...styles, ...customStyles }}
      onRequestClose={onClose}
      {...rest}
    >
      {children}
    </ReactModal>
  );
}

export default Modal;
