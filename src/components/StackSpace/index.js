import React from 'react';
import styled from 'styled-components';

const WordWrap = styled.div`
  border: 2px solid black;
  padding: 5px;
  text-align: center;
  margin-right: 5px;
`;

const Word = styled.p`
  color: black;
  letter-spacing: 2px;
  text-transform: uppercase;
`;

const StackContainer = styled.div`
  padding: 10px;
  background-color: #eee;
  display: flex;
`;

function StackSpace({ words }) {
  return (
    <StackContainer>
      {words.map((word, index) => (
        <WordWrap key={index}>
          <Word>{word}</Word>
        </WordWrap>
      ))}
    </StackContainer>
  );
}

export default StackSpace;
