import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { fetchRequest } from '../../utils/fetch';
import Loader from '../Loader';

const Flex = styled.div`
  display: flex;
  align-items: ${({ alignItems }) => alignItems || 'center'};
  justify-content: ${({ justifyContent }) => justifyContent || 'flex-start'};
  width: ${({ width }) => width || '100%'};
  flex-direction: ${({ flexDirection }) => flexDirection || 'row'};
  margin: ${({ margin }) => margin};
`;

const Card = styled.div`
  display: flex;
  padding: 20px;
  width: 100%;
  background-color: #fafafa;
  box-shadow: -1px 4px 3px -3px rgba(0, 0, 0, 0.75);
  margin-top: 10px;
`;

function Leaderboard() {
  const [avgScore, setAvgScore] = useState(0);
  const [totalGames, setTotalGames] = useState(0);
  const [maxLevel, setMaxLevel] = useState(0);
  const [topTen, setTopTen] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    async function fetchLeaderBoard() {
      const leaderboard = await fetchRequest(
        `${process.env.REACT_APP_API}/leaderboard`,
        'GET'
      );

      setLoading(false);
      setAvgScore(Math.floor(leaderboard.stats.score));
      setTotalGames(leaderboard.stats.totalGames);
      setMaxLevel(leaderboard.stats.maxLevel);
      setTopTen(leaderboard.topTen);
    }

    fetchLeaderBoard();
  }, []);

  if (loading) {
    return <Loader />;
  } else {
    return (
      <Flex width="50%" flexDirection="column" margin="20px 0">
        <Flex justifyContent="center">
          <Flex justifyContent="center">
            <p>Average Score: {avgScore}</p>
          </Flex>
          <Flex justifyContent="center">
            <p>Maximum Level: {maxLevel}</p>
          </Flex>
          <Flex justifyContent="center">
            <p>Total games: {totalGames}</p>
          </Flex>
        </Flex>
        <Flex flexDirection="column">
          {topTen.map((game, index) => (
            <Card key={index}>
              <Flex flexDirection="column" alignItems="flex-start">
                <h4>{game.user.name}</h4>
                <p>Level: {game.level}</p>
              </Flex>
              <Flex justifyContent="flex-end">
                <h6>{game.score}</h6>
              </Flex>
            </Card>
          ))}
        </Flex>
      </Flex>
    );
  }
}

export default Leaderboard;
