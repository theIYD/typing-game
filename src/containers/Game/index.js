import React, { useState } from 'react';
import styled from 'styled-components';
import { generateWords } from '../../utils/words';
import { currentTime } from '../../utils/time';
import useKeyPress from '../../hooks/useKeyPress';
import { fetchRequest } from '../../utils/fetch';

import StackSpace from '../../components/StackSpace';
import Stat from '../../components/Stat';
import Button from '../../components/Button';
import Leaderboard from '../../components/Leaderboard';

const Flex = styled.div`
  display: flex;
  align-items: center;
  width: auto;
  flex-direction: ${({ flexDirection }) => flexDirection || 'row'};
  margin: ${({ margin }) => margin};
`;

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

const CurrentChar = styled.p`
  font-size: 4rem;
  font-weight: bold;
  text-align: center;
`;

function Game() {
  const [stackSpace, setStackSpace] = useState(generateWords());
  const [currentWord, setCurrentWord] = useState(stackSpace[0]);
  const [outgoingChars, setOutgoingChars] = useState('');
  const [currentChar, setCurrentChar] = useState(currentWord.charAt(0));
  const [incomingChars, setIncomingChars] = useState(currentWord.substr(1));
  const [startTime, setStartTime] = useState();
  const [duration, setDuration] = useState(0);
  const [multiplier, setMultiplier] = useState(1);
  const [score, setScore] = useState(0);
  const [level, setLevel] = useState(1);
  const [isGame, setIsGame] = useState();
  const [showLeaderboard, setShowLeaderboard] = useState(false);

  const leaderboard = () => setShowLeaderboard(!showLeaderboard);

  const startGame = () => {
    setIsGame(1);
    let interval = setInterval(() => {
      let newWord = generateWords(1);
      setStackSpace((stackSpace) => {
        if (stackSpace.length < 8) {
          return [...stackSpace, ...newWord];
        } else {
          // Loses the game
          clearInterval(interval);
          setIsGame(0);
          return [...stackSpace];
        }
      });
    }, 2000);
  };

  const saveScore = async (e) => {
    e.preventDefault();

    const data = await fetchRequest(
      `${process.env.REACT_APP_API}/score`,
      'POST',
      { score, level }
    );

    if (data.error === 0) {
      alert(data.message);
    }
  };

  useKeyPress((key) => {
    if (isGame) {
      if (!startTime) {
        setStartTime(currentTime());
      }

      let updatedOutgoingChars = outgoingChars;
      let updatedIncomingChars = incomingChars;

      // Key matches
      if (key === currentChar) {
        updatedOutgoingChars += currentChar;
        setOutgoingChars(updatedOutgoingChars);

        setCurrentChar(incomingChars.charAt(0));

        updatedIncomingChars = incomingChars.substring(1);
        setIncomingChars(updatedIncomingChars);
      } else {
        // Reset back if a wrong key does not match
        setMultiplier(1);
      }

      // Do the following after a space is pressed for a correct word
      if (
        key === ' ' &&
        incomingChars === '' &&
        currentWord === outgoingChars
      ) {
        // Calculate the score
        let calculatedScore = currentWord.length * multiplier * level;
        setScore(score + calculatedScore);
        setMultiplier(multiplier + 1);

        // Increase the level if time spent on current word was less than the time spent on previous word.
        let timeSpent = currentTime() - startTime;
        if (timeSpent < duration) {
          setLevel((level) => level + 1);
        }
        setDuration(timeSpent);
        setStartTime(0);

        // word typed was correct
        stackSpace.shift();
        setStackSpace([...stackSpace]);
        setCurrentWord(stackSpace[0]);
        setOutgoingChars('');
        setCurrentChar(stackSpace[0].charAt(0));
        setIncomingChars(stackSpace[0].substr(1));
      }
    }
  });

  return (
    <Container>
      <Flex>
        <Stat
          css={{ top: '10px' }}
          stat="level"
          data={{ text: 'level', stat: level }}
        />
        <Stat
          css={{ top: '10px' }}
          stat="score"
          data={{ text: 'score', stat: score }}
        />
        <Stat
          css={{ top: '20px' }}
          stat="multiplier"
          data={{ text: 'multiplier', stat: `${multiplier}X` }}
        />
      </Flex>
      <Flex>
        <StackSpace words={stackSpace} />
      </Flex>
      <Flex margin="0.5rem 0 0 0">
        <Button onClick={leaderboard}>Leaderboard</Button>
        <Button onClick={startGame} disabled={showLeaderboard}>
          Start
        </Button>
        {isGame === 0 ? <Button onClick={saveScore}>Save</Button> : null}
      </Flex>
      {isGame === 1 ? (
        <Flex>
          <CurrentChar>{currentChar}</CurrentChar>
        </Flex>
      ) : null}
      {showLeaderboard ? <Leaderboard /> : null}
    </Container>
  );
}

export default Game;
