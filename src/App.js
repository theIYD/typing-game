import React, { useState } from 'react';
import styled from 'styled-components';

import Game from './containers/Game';
import User from './components/User';

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
`;

function App() {
  const [modalOpen, setModalOpen] = useState(true);
  const token = localStorage.getItem('token');
  const closeModal = () => {
    setModalOpen(false);
  };

  return (
    <Container>
      {!token ? <User isOpen={modalOpen} onClose={closeModal} /> : null}
      <Game />
    </Container>
  );
}

export default App;
