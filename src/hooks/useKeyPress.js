import { useEffect, useState } from 'react';

const useKeyPress = (cb) => {
  const [keyPressed, setKeyPressed] = useState();

  useEffect(() => {
    const keyDownHandler = ({ key }) => {
      if (keyPressed !== key && key.length === 1) {
        setKeyPressed(key);
        cb && cb(key);
      }
    };

    const keyUpHandler = () => {
      setKeyPressed(null);
    };

    window.addEventListener('keydown', keyDownHandler);
    window.addEventListener('keypress', keyUpHandler);

    return () => {
      window.removeEventListener('keydown', keyDownHandler);
      window.removeEventListener('keyup', keyUpHandler);
    };
  });

  return keyPressed;
};

export default useKeyPress;
