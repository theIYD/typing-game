import faker from 'faker';

export const generateWords = (numberOfWords = 3) => {
  return new Array(numberOfWords)
    .fill()
    .map((_holder) => faker.random.word().toLowerCase());
};
