export const fetchRequest = async (url = '', method = 'POST', data = {}) => {
  const config = {
    method,
    mode: 'cors',
    headers: {
      'Content-Type': 'application/json',
    },
  };

  if (method !== 'GET' && data) config['body'] = JSON.stringify(data);

  const token = localStorage.getItem('token');
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }

  const response = await fetch(url, config);
  return response.json();
};
